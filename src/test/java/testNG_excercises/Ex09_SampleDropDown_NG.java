package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex09_SampleDropDown_NG {
  WebDriver driver = null;
	@Test
  public void f() {
		String expectedText = "CP-AAT";
		String expectedText2 = "CP-MLDS";
		int counter = 0;
		driver.get("file:///C:/Users/jeyan/eclipse-workspace/mavenCPSATonlineJUL/src/test/resources/data/dropdown.html");
		while (counter <=1){
			driver.findElement(By.xpath("/html/body/form/select")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			List<WebElement> cpaat_element = driver.findElements(By.xpath("/html/body/form/select/option"));
		
			for(WebElement ele : cpaat_element) {
				
				String text = ele.getText();
				System.out.println(text);
							  
				if(text.contains(expectedText) & (counter ==0)) {
					ele.click();
					System.out.println("CP-AAT selected");	
					counter++;
				}
				if(text.contains(expectedText2) & (counter ==1)) {
					ele.click();
					System.out.println("CP-MLDS selected");	
					counter++;
				}
			}
						
		}
  }
  @BeforeTest
  public void beforeTest() {
	  driver = utils.HelperFunctions.createAppropriateDriver("Firefox");
	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
