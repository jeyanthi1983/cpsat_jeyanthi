package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Pepperfry_mockExam {
	WebDriver driver = null;
	
	@BeforeTest
	  public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		  ChromeOptions options = new ChromeOptions();
		 
		  options.setExperimentalOption("excludeSwitches", Arrays.asList("disable-popup-blocking"));

		  options.addArguments("--disable-popup-blocking");

		  driver = new ChromeDriver(options);
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  }
  @Test(dataProvider = "dp")
  public void Pepperfry(  String Stext) {
	  
	  String SearchText = Stext;
	  int counterFlag = 0;
	  int noproductsFlag =0;
	  ArrayList<Integer> originalPrice;// = new ArrayList(iSize);
	  By noresult = By.xpath("//*[@id=\"filterContent\"]/div[2]/div/div/div/div/div/span");
	  By price = By.xpath("//*/span[contains(@class,'clip-offr-price ')]");
	  By searchbtn = By.id("searchButton");
	  By searchfield = By.cssSelector("#search.ui-autocomplete-input");
	  By sorttype = By.id("curSortType");
	  By LowtoHIgh = By.xpath("//*[@id=\"sortBY\"]/li[2]/a");
	  By proPrice = By.xpath("//*/span[contains(@class,'clip-offr-price')]");
	  
	  driver.navigate().to("https://www.pepperfry.com/");
	 
		 
	  WebElement search = driver.findElement(searchfield);
	  search.clear();
	  search.sendKeys(SearchText);
	  search.sendKeys(Keys.ENTER);
	  WebElement searchbutton = driver.findElement(searchbtn);
	  searchbutton.click();
	 
	  WebDriverWait wait = new WebDriverWait(driver, 20);
	
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(price));
			
		}
		catch(Exception e) {
			
			WebElement NoResults = driver.findElement(noresult);
			
			String spanText = NoResults.getText();
			if(spanText.contains("No Results Found For"))
			{
				System.out.println("No Results found for:  "+ SearchText);
				counterFlag =3;
				noproductsFlag = 1;
				
			}
			else
			 System.out.println("** Error **" + e.getMessage());
			
		}

if(noproductsFlag ==0) {
	
		 List<WebElement> OriginalPriceList = driver.findElements(price);
			 int iSize = OriginalPriceList.size();
			 System.out.println("Size of all priceList :" + Integer.toString(iSize));
			 originalPrice = new ArrayList(iSize);
			 for(int i=0;i<iSize;i++)
			 {
				 
				 WebElement li = OriginalPriceList.get(i);
				 String priceTag = li.getText().replaceAll("[^0-9]","");
				 int pricevalue = Integer.parseInt(priceTag);
				 originalPrice.add(pricevalue);
				 
				
			 }
			 System.out.println("Price list before comparing with the collection sort method");
				for(int obj: originalPrice)  {  
					System.out.print(obj+"\n"); 
				}
			 Collections.sort(originalPrice);
			 driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
}
	  while(counterFlag<3) {
		  
		  try {
				driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
				
				
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(sorttype));
		    
			 	WebElement cursorsort =  driver.findElement(sorttype); 
			 	Actions actions = new Actions(driver);
			    actions.moveToElement(cursorsort).perform();
			    actions.moveToElement(cursorsort).click().perform();
			
			    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(LowtoHIgh));
			    WebElement sortby = driver.findElement(LowtoHIgh);
				 actions.moveToElement(sortby).build().perform();
				 actions.moveToElement(sortby).click().perform();
				
				counterFlag =3;
			
		  } catch(Exception e) {
			
					
					WebElement NoResults = driver.findElement(noresult);
					wait.until(ExpectedConditions.presenceOfElementLocated(noresult));
					String spanText = NoResults.getText();
					if(spanText.contains("No Results Found For"))
					{
						System.out.println("No Results found for:  "+ SearchText);
						counterFlag =3;
						break;
					}
					else {
							System.out.println(e.getMessage());
							System.out.println("Unable to click sorting :" + counterFlag );
							counterFlag++;
						}
			}
		  System.out.println("Search Text : " + SearchText +" Done :" + counterFlag +"\n"); 
		}
	
	if(noproductsFlag==0) {
		WebElement Element = driver.findElement(proPrice);
		
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(Element)));
			
		}
		catch(Exception e) {
			System.out.println("** Error **" + e.getMessage());
			
		}
		List <WebElement> actualPrice = driver.findElements(proPrice);
		int isize = actualPrice.size();	

		System.out.println("iSize = " + isize);
		
		ArrayList<Integer> actualSortedPriceList = new ArrayList<Integer>(isize); 

		for(int i=0;i<isize;i++) {
			WebElement wePrice = actualPrice.get(i);
			String strPrice = wePrice.getText().replaceAll("[^0-9]","");
			int iPrice = Integer.parseInt(strPrice);
			actualSortedPriceList.add(iPrice);

		}
		System.out.println("");
		ArrayList<Integer> finalsortedList = new ArrayList(isize);
		finalsortedList = actualSortedPriceList;

		Collections.sort(finalsortedList);
		System.out.println("Price list After using sort ");
		for(int obj1:actualSortedPriceList)  {
			System.out.print(obj1 + "\n"); 
			
		}
	
		System.out.println("");
		try {
			Assert.assertEquals(actualSortedPriceList,finalsortedList);
		}catch(AssertionError e) {
			System.out.println("Sort By feature 'Low to High' is defective");
		}
	}

  }
  
  @DataProvider
  public String[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException  {
	  
	  String[][] data = utils.XLDataReaders.getExcelData("C:\\Users\\jeyan\\eclipse-workspace\\mavenCPSATonlineJUL\\src\\test\\resources\\data\\pepperfy.xlsx", "data");
	  return data;
	  
  }	
 
  

  @AfterTest
  public void afterTest() {
  driver.quit();
	
  }

}
