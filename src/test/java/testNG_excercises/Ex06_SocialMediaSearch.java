package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex06_SocialMediaSearch {
	WebDriver driver = null;
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Firefox",false);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  }

	@Test(dataProvider = "dp")
  public void f(Integer n, String s) {
		driver.get("http://www.agiletestingalliance.org");
		
		String filename = "C:\\Users\\jeyan\\eclipse-workspace\\mavenCPSATonlineJUL\\src\\test\\resources\\screenshots\\SocialMediaImage1.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
		////*[@id="custom_html-10"]/div/ul/li[1]/a  linkedIN
		////*[@id="custom_html-10"]/div/ul/li[3]/a youtube
		////*[@id="custom_html-10"]/div/ul/li[2]/a twitter
		//*[@id="custom_html-10"]/div/ul/li[4]/a insta
		//*[@id="custom_html-10"]/div/ul/li[5]/a facebook
		//*[@id="custom_html-10"]/div/ul/li[6]/a whatsapp
		//*[@id="custom_html-10"]/div/ul/li[7]/a telegram
		
		//*[@id="custom_html-10"]/div/ul/li/a  7 elements
		By byvar = By.xpath("//*[@id=\"custom_html-10\"]/div/ul/li/a");
		List<WebElement> weList = driver.findElements(byvar);
		for (WebElement ele : weList) {
			String href = ele.getAttribute("href");
			System.out.println ("Href is : " + href);
		}
		
  }

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },
    };
  }
  
  @AfterTest
  public void afterTest() {
  }

}
