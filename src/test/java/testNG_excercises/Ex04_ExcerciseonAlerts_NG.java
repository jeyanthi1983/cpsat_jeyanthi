package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex04_ExcerciseonAlerts_NG {
  WebDriver driver = null;
	
  @BeforeTest
 public void beforeTest() {
		driver = utils.HelperFunctions2.createAppropriateDriver("Chrome");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
 }
	
	@Test
  public void f() {
		
		int counter =0;
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		
		/*
		 * 2a. Click on JS Alert and 
    Click ok button on Alert 
    Validate Result: "You successfully clicked an alert"
    		 */
		By byvar = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
		WebElement jsalert = driver.findElement(byvar);
		jsalert.click();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		WebElement textresult = driver.findElement(By.id("result"));
		String text = textresult.getText();
		
		if(text.contains("You successfuly clicked an alert")) {
			System.out.println("Got text verified for "+ text);
			
		}
	
	
	
		/* 2b. Click for JS Confirm
	i) click Ok and validate Result
	ii)click cancel and validate result
		 */
		By byvar1 = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
		WebElement jsconfirm = driver.findElement(byvar1);
		jsconfirm.click();
		
		Alert alert1 = driver.switchTo().alert();
		alert1.accept();
		
		
		String text1 = textresult.getText();
		
		if(text1.contains("You clicked: Ok")) {
			System.out.println("Got text verified for "+ text1);
			
		}
		jsconfirm.click();
		alert1.dismiss();
String text2 = textresult.getText();
		
		if(text2.contains("You clicked: Cancel")) {
			System.out.println("Got text verified for " + text2);
			
		}
	
	
	/* 2c. Click for JS Prompt and then
	i) Input your name and click ok to validate result
	ii) Input your name and click cancel to validate result
	iii) No Input on JS Prompt and click ok to validate result
	iv)  No input on JS Prompt and click cancel to validate result 
	*/
		
			WebElement jsprompt = driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button"));
			jsprompt.click();
			
			Alert alert_jsprompt = driver.switchTo().alert();
			alert_jsprompt.sendKeys("Jeyanthi");
			if (counter ==0) {
				alert_jsprompt.accept();
				
				String text3 = textresult.getText();
				
				if(text3.contains("You entered: Jeyanthi")) {
					System.out.println("Got text verified for " + text3);
				counter++;
				
				}
			}
		jsprompt.click();
			
			Alert alert_jsprompt1 = driver.switchTo().alert();
			alert_jsprompt1.sendKeys("Jeyanthi");
			
			alert_jsprompt1.dismiss();
			
			String text4 = textresult.getText();
			
			if(text4.contains("You entered: null")) {
				System.out.println("Got text verified for " + text4);
				
			}
			
		jsprompt.click();
			
			Alert alert_jsprompt2 = driver.switchTo().alert();
				
			alert_jsprompt2.accept();
			
			String text5 = textresult.getText();
			
			if(text5.contains("You entered:")) {
				System.out.println("Got text verified for " + text5);
				
			}
			
		jsprompt.click();
			
			Alert alert_jsprompt3 = driver.switchTo().alert();
				
			alert_jsprompt3.dismiss();
			
			String text6 = textresult.getText();
			
			if(text6.contains("You entered: null")) {
				System.out.println("Got text verified for " + text6);
				
			}
			
		
  }
  

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
