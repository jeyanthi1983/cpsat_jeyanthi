package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class Mock_exam1 {
  
	 @Test(groups = "selenium")
	public void t1(){
	System.out.print("This is a Selenium test. ");
	}
	/*@Test(groups = "qtp")
	public void t2(){
	System.out.print("This is a QTP test. ");
	}
	*/
	@Test(groups = {"qtp","Junit"})
	public void t3(){
	System.out.print("This is a QTP and a Junit test. ");
	}


}
