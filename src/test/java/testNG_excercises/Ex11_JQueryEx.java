package testNG_excercises;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Ex11_JQueryEx {
	WebDriver driver = null;
	By frameLocator = By.className("demo-frame");
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	  }
	
	
	@Test
  public void AutoComplete() throws InterruptedException {
		driver.get("https://jqueryui.com/autocomplete/");
		WebElement frameElement=driver.findElement(frameLocator);
		/*JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tags")));
		*/
		 driver.switchTo().frame(frameElement);
		Thread.sleep(3000);
		WebElement inputbox = driver.findElement(By.id("tags"));
		inputbox.click();
		inputbox.sendKeys("J");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		Actions builder = new Actions(driver);

		 //driver.switchTo().frame(0);
		WebElement javaSelect = driver.findElement(By.cssSelector("#ui-id-3.ui-menu-item-wrapper"));
		builder.moveToElement(javaSelect).build().perform();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(javaSelect));
		builder.click(javaSelect).build().perform();
		/*
		List<WebElement> options = javaSelect.findElements(By.tagName("li"));
		
		for(WebElement value : options) {
		if(value.getText().contains("Java")) {
			System.out.println("Trying to select based on Text : Java ");
	           value.click();
		}
		}*/
		/*for(WebElement value : options) {
			System.out.println("Inside for loop");
			/*if(value.getText().contains("Java"))
				{value.click();
			break;}
		
			if(2 <=options.size()) {
				System.out.println("Trying to select based on index:2 ");
		           options.get(2).click();
			}
		}
			*/
		//WebElement inputbox = driver.findElement(By.id("tags"));
		inputbox.clear();
		inputbox.sendKeys("J");
		try {
			  /* WebElement autoOptions = driver.findElement(By.id("ui-id-1"));
			   wait.until(ExpectedConditions.visibilityOf(autoOptions));

			   List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li")); 
			   */
			List<WebElement> optionsToSelect = driver.findElements(By.xpath("//*[@id=\"ui-id-1\"]/li"));
			   for(WebElement option : optionsToSelect){
				   System.out.println(option.getText());
				   
			      }
			   
			  } catch (NoSuchElementException e) {
			   System.out.println("No Such element found :" + e.getStackTrace());
			  }
			  catch (Exception e) {
			   System.out.println("Exception with : "+ e.getStackTrace());
			  }
			 
		
  }
  
  @AfterTest
  public void afterTest() {
  }

}
