package jUnit_excercises;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class Ex03_Annauniv_JU {

	WebDriver driver = null;
	@BeforeEach
	void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Firefox", false);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Annauniv_JU execution Done. \n");
		driver.quit();
	}

	@Test
	void test() {
		driver.get("https://www.annauniv.edu/");
		WebDriverWait wait = new WebDriverWait(driver,25);
		
		// driver.findElement(By.cssSelector("br--:nth-child(1)")).click();
		    // 4 | click | linkText=Departments | 
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Departments")));
		
		WebElement ele = driver.findElement(By.linkText("Departments"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ele);
		System.out.println("page title: "+driver.getTitle());
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id=\'link3\']/strong")));
	    WebElement element = driver.findElement(By.xpath("//a[@id=\'link3\']/strong"));
	    Actions builder = new Actions(driver);
	    builder.moveToElement(element).perform();
	    WebElement ele1 = driver.findElement(By.id("menuItem32"));
	    Actions builder1 = new Actions(driver);
	    builder1.moveToElement(ele1).perform();
	    driver.findElement(By.id("menuItem32")).click();
	    System.out.println("page title: "+driver.getTitle());
	    // 8 | mouseOver | id=link3 | 
	    {
	    	wait.until(ExpectedConditions.elementToBeClickable(By.id("link3")));
	    	WebElement element3 = driver.findElement(By.id("menuItem14"));
	      WebElement element1 = driver.findElement(By.id("link3"));
	      Actions builder2 = new Actions(driver);
	      builder2.moveToElement(element1).moveToElement(element3).click().perform();
	    }
	    System.out.println("page title: "+driver.getTitle());
	  
	}

}
