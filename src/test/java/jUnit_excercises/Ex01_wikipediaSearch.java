package jUnit_excercises;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import junit.framework.Assert;

public class Ex01_wikipediaSearch {

	WebDriver Driver = null;
	@Before
	public void setUp() throws Exception {
		Driver = utils.HelperFunctions.createAppropriateDriver("Firefox", false);
		Driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Wikipedia search execution Done. \n");
		Driver.quit();
	}

	@Test
	public void test() {
		Driver.get("https://www.wikipedia.org/");
		
		Driver.findElement(By.xpath("//*[@id=\"js-link-box-en\"]")).click();
		
		Driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		if(Driver.findElement(By.xpath("//*[@id=\"mp-welcome\"]")) != null)
			System.out.println("Browser navigated to welcome page");
		else 
		{
			System.out.println("Something went wrong !");
			Driver.quit();
		}
		Driver.findElement(By.xpath("//*[@id=\"searchInput\"]")).sendKeys("Selenium");
		
		Driver.findElement(By.id("searchButton")).click();
		String pageTitle = Driver.getTitle();
		
		System.out.println(pageTitle );
		
		String ExpectedTitle = "Selenium - Wikipedia";
		
										
		//assertEquals(pageTitle,ExpectedTitle);
		Assert.assertEquals(ExpectedTitle, pageTitle);
		
	

	}

}
