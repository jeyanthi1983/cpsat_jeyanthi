package jUnit_excercises;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AutocompletetestTest.class, Ex01_Google.class, Ex01_GoogleNavigation_ju.class,
		Ex01_wikipediaSearch.class, Ex01A_wikipediaNavigation_ju.class })
public class AllTests {

}
