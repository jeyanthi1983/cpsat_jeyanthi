package jUnit_excercises;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex01A_wikipediaNavigation_ju {
	WebDriver driver = null;

	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Firefox", true);
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Wikipedia Navigation execution Done. \n");
		driver.quit();
		
	}

	@Test
	public void test() {
				driver.get("https://www.wikipedia.org/");
				driver.findElement(By.xpath("//*[@id=\"js-link-box-en\"]")).click();
				
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				
				if(driver.findElement(By.xpath("//*[@id=\"mp-welcome\"]")) != null)
					System.out.println("Browser navigated to welcome page");
				else 
				{
					System.out.println("Something went wrong !");
					driver.quit();
				}
				driver.findElement(By.xpath("//*[@id=\"searchInput\"]")).sendKeys("Selenium");
				
				driver.findElement(By.id("searchButton")).click();
				
				System.out.println("The Page 3 Title is " + driver.getTitle());
				driver.navigate().back();
				System.out.println("The Page 2 Title is " + driver.getTitle());
				driver.navigate().forward();
				System.out.println("Again Page 3 Title is " + driver.getTitle());
					}

}
