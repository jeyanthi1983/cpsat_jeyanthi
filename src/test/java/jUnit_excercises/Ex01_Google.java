package jUnit_excercises;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Ex01_Google {
WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Firefox");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
	}

	@After
	public void tearDown() throws Exception {
		  System.out.println("GOogle Java execution Done. \n");
		//driver.quit();
	}

	@Test
	public void test() {
		driver.get("http://www.google.com");
	//	driver.get("http://www.softwaretestingmaterial.com");
		System.out.println("Page title is : "+ driver.getTitle());
	}

}
