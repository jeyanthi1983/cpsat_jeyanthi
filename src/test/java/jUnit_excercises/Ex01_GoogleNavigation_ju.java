package jUnit_excercises;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex01_GoogleNavigation_ju {
	WebDriver Driver = null;			

	@Before
	public void setUp() throws Exception {
		Driver = utils.HelperFunctions.createAppropriateDriver("Firefox");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Google Navigation execution Done. \n");
		Driver.quit();
	}

	@Test
	public void test() {
		Driver.get("http://www.google.com");
		
		System.out.println("page 1 Title is " + Driver.getTitle());
		
		By byvar = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input");
		WebElement weSearch = Driver.findElement(byvar);
		
		weSearch.sendKeys("CPSAT");
		
		weSearch.sendKeys(Keys.ENTER);
		System.out.println("page 2 Title is " + Driver.getTitle());
		////*[@id="rso"]/div[1]/div/div[1]/a/h3
		
		By byvar2 = By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3");
		WebElement welink = Driver.findElement(byvar2);
		welink.click();
		
		System.out.println("page 3 Title is " + Driver.getTitle());
		Driver.navigate().back();
		
		System.out.println("page 3-back Title(page 2) is " + Driver.getTitle());
		Driver.navigate().forward();

		System.out.println("again page 3 Title is " + Driver.getTitle());
		
		
	}

}
