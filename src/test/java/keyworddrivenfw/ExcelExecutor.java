package keyworddrivenfw;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

import java.sql.Timestamp;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



/**
 * this is the excel executor
 * goes row by row of excel
 * reads the keywords in excel sheet
 * executes one by one
 *
 */
public class ExcelExecutor {

	WebDriver driver;
	Sheet excelSheet;
	@BeforeTest
	public void setUp() throws Exception {
		java.util.Date date= new java.util.Date();
		System.out.println("\n\nExecution Log - Start Time - " + new Timestamp(date.getTime()));
		System.out.println("Launching google chrome with new profile..");
		driver = utils.HelperFunctions2.createAppropriateDriver("chrome");
	}

	@Test
	public void testLogin() throws Exception {

		GetByObjectAndAct getAndAct = new GetByObjectAndAct(driver);
		//Read keyword sheet
		excelSheet = ReadExcelFileSheet.getExcelSheet("src/test/java/keyworddrivenfw/","TestCase.xlsx" , "Frameworksheet");
		//Find number of rows in excel file
		int rowCount = excelSheet.getLastRowNum()-excelSheet.getFirstRowNum();
		//Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount+1; i++) {
			//Loop over all the rows
			Row row = excelSheet.getRow(i);
			//Check if the first cell contain a value, if yes, That means it is the new testcase name
			try {

				if(row.getCell(0).toString().length()==0){
					//Print testcase detail on console
					System.out.println(row.getCell(1).toString()+"----"+ row.getCell(2).toString()+"----"+
							row.getCell(3).toString()+"----"+ row.getCell(4).toString());

					//Call perform function to perform operation on UI
					try {

						getAndAct.performAction(row.getCell(1).toString(), row.getCell(2).toString(),
								row.getCell(3).toString(), row.getCell(4).toString());

					}
					catch(Exception e){
						System.out.println("fail =" + e.getMessage());
					}//end catch
				}//end if
				else{
					//Print the new  testcase name when it started
					System.out.println("New Testcase->"+row.getCell(0).toString() +" Started");
				}//end else

			}
			catch(Exception e) {
				System.out.println("handling null pointer exception");
			}
		}//end for

	}

	@AfterTest
	public void tearDown(){

		driver.quit();
		java.util.Date date= new java.util.Date();
		System.out.println("\n\nExecution Log - End Time - " + new Timestamp(date.getTime()));

	} 
}
