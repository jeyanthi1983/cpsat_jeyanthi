package sequencingNG;

import org.testng.annotations.Test;

public class TestSequence {
	
  @Test (priority=0)
  public void first() {
	  System.out.println("INSIDE Method No. ONE");
  }
  
  @Test(priority=0)
  public void second() {
	  System.out.println("INSIDE Method No. TWO");
  }
  
  @Test()
  public void third() {
	  System.out.println("INSIDE Method No. THREE");
  }
  
  @Test(priority=0)
  public void fourth() {
	  System.out.println("INSIDE Method No. FOUR");
  }
  
  @Test(priority=8)
  public void fifth() {
	  System.out.println("INSIDE Method No. FIVE");
  }
  
  
  
  
}
