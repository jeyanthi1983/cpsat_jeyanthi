package cpsatmock2;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class firstcry_ng {
	WebDriver Driver = null;
	 WebDriverWait wait = null;
	 JavascriptExecutor js;
	 @BeforeTest
	  public void beforeTest() {
		 System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
		 ChromeOptions options = new ChromeOptions();
		 options.addArguments("--disable-notifications");
		 options.addArguments("--disable-popup-blocking");
		 
		 Driver = new ChromeDriver(options);
		 Driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 wait = new WebDriverWait(Driver, 25);
		 Driver.manage().window().maximize();
	 }
  @Test(dataProvider = "dp")
  public void test(String s) {  
	    String ExpecTitle = "Kids Footwear - Buy Baby Booties, Boys Shoes, Girls Sandals Online India";
	  	Driver.get("https://www.firstcry.com/");
	   // Driver.manage().window().setSize(new Dimension(874, 728));
	    
	   // Driver.switchTo().defaultContent();
	  	String MainTitle = Driver.getTitle();
		 System.out.println("Main title is " + MainTitle);
	    Driver.findElement(By.id("search_box")).click();
	    Driver.findElement(By.id("search_box")).clear();;
	    Driver.findElement(By.id("search_box")).sendKeys(s);
	    Driver.findElement(By.cssSelector(".search-button")).click();
	    String ActTitle = Driver.getTitle();
		 System.out.println("The page title is " + ActTitle);
		if(ActTitle.contains(ExpecTitle)){
			
				  
				  JavascriptExecutor jse = (JavascriptExecutor) Driver;    
				  WebElement viewAll = Driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[2]/div[8]/div[2]/span/a"));
				  jse.executeScript("arguments[0].scrollIntoView(true);", viewAll);
				  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'View All Products')]")));
				  viewAll.click();
			  }
			  
				
	    {
	      WebElement element = Driver.findElement(By.cssSelector(".sort-select-content"));
	      Actions builder = new Actions(Driver);
	      builder.moveToElement(element).perform();
	    }
	    {
	      WebElement elem = Driver.findElement(By.linkText("Price"));
	      Actions Builder = new Actions(Driver);
	      Builder.moveToElement(elem).perform();
	    }
	    Driver.findElement(By.linkText("Price")).click();
	    //Driver.findElement(By.linkText("Price")).click();
	    js.executeScript("window.scrollTo(0,122)");
	    js.executeScript("window.scrollTo(0,936)");
	    Driver.findElement(By.linkText("Price")).click();
	    Driver.findElement(By.cssSelector(".sort-select-content")).click();
	    Driver.findElement(By.linkText("Price: High To Low")).click();
	   // Driver.findElement(By.cssSelector(".logo_head")).click();
  }

  @DataProvider
  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
	  String[][] data;
		
	   data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\firstcrydata.xlsx", "data");


return data;
  }
  

  @AfterTest
  public void afterTest() {
	  Driver.quit();
  }

}
