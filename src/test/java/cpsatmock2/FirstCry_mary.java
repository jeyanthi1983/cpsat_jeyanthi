package cpsatmock2;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class FirstCry_mary {
	
	WebDriver Driver = null;
	

	  @DataProvider
	  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		  
	    String [] [] data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\firstcrydata.xlsx", "data");
		return data;
		  
		  
	   
	    };
	
	
	
  @BeforeTest
  public void beforeTest() {
	  
	  Driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  
	  
  }
	  
	  @Test (dataProvider = "dp")
	  public void f(String sProd) throws InterruptedException {
		  
		  String sProductFC = sProd;
		
		  Driver.get("https://www.firstcry.com/");
		  
		//Searches by product
		  By Var1 = By.xpath("//*[@id=\"search_box\"]");
		  WebElement We1 = Driver.findElement(Var1);
		  We1.clear();
		  We1.sendKeys(sProductFC);
		  We1.sendKeys(Keys.ENTER);
		  System.out.println("The product entered is " + sProductFC);
		  
		  
		  //Selects High to Low price link
		  Actions action = new Actions (Driver);
      	By Var12 = By.xpath("//*[@class=\"sort-select-content L12_42\"]");
		WebElement We12 = Driver.findElement(Var12);
		By Var13 = By.xpath("//*[@class =\"na\" and contains (text(),'Price')]");
		WebElement We13 = Driver.findElement(Var13);
		action.moveToElement(We12).moveToElement(We13).click().perform();
		By Var14 = By.xpath("//*[@class = \"na\" and contains (text(),'Price: High To Low')]");
		WebElement We14 = Driver.findElement(Var14);
		action.moveToElement(We14).click().perform();
	
		
	   //Gets the price values into a list
		By Var15 = By.xpath("//*[@class=\"r1 B14_42\"]");
		List <WebElement> We15 = Driver.findElements(Var15);
		WebDriverWait wait = new WebDriverWait (Driver,8);
		wait.until(ExpectedConditions.visibilityOfAllElements(We15));
		
	   
		
		
		//prints the first 8 prices
	   	//Converts string to float and stores in new list for checking descending order
		List <Float> lsNewPrice = new ArrayList<Float>();
		for (int iCounter=0; iCounter< 8;iCounter++) {
			String sPriceText = We15.get(iCounter).getText();
			System.out.println (sPriceText);
			float f=Float.parseFloat(sPriceText);
			lsNewPrice.add(f);
			
					
		}
		
		
		
		
		// Iterates through float list to check if its descending 
		float FirstValue = 0 ;
		float PrevValue =0;
		for (int iCounter2 =0; iCounter2 <8;iCounter2 ++) 
		
		{
			
			
		if (iCounter2 ==0) {
		     FirstValue = lsNewPrice.get(iCounter2);
		     System.out.println("The price value of item "+ iCounter2 +" is " + lsNewPrice.get(iCounter2));
			}
			
	   
				
	   if (iCounter2 >1) {
					if (lsNewPrice.get(iCounter2)<=PrevValue ) {
						System.out.println("The value "+lsNewPrice.get(iCounter2)+ " is less or equal  to "+PrevValue);
					}
						else {
							
							System.err.println ("The sort order is incorrect "+ lsNewPrice.get(iCounter2));
						}
	   }
	   
	   
	   
	   
	   if (iCounter2==1) {
			if (lsNewPrice.get(iCounter2)<=FirstValue ) {
				System.out.println("The value "+lsNewPrice.get(iCounter2)+ " is less than "+FirstValue );
			}
				else {
					
					System.err.println ("The sort order is incorrect "+lsNewPrice.get(iCounter2) );
				}
   
			
			
			
	  }	
	   
	 PrevValue =	lsNewPrice.get(iCounter2);
		}
				
				
	  } 		
						


  @AfterTest
  public void afterTest() {

	    
	  Driver.quit();
  }

}
