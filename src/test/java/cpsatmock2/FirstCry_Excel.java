package cpsatmock2;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class FirstCry_Excel {
	WebDriver driver=null;
	WebDriverWait wait=null;
	JavascriptExecutor jse;
	
	
  //@SuppressWarnings("unchecked")
@Test(dataProvider = "dp")
  public void firstCry(String data) {
	  
	  String expURLForShoeSearch="https://www.firstcry.com/baby-kids-footwear";
	  String filename = "src/test/resources/firstcryPoonam.png";
	  driver.get("https://www.firstcry.com/");
	  driver.findElement(By.xpath("//*[@id=\"search_box\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"search_box\"]")).sendKeys(data, Keys.ENTER);
	  
	  String actualUrl=driver.getCurrentUrl();
	  
	  if(actualUrl.contains(expURLForShoeSearch)) {
		    
		  //WebElement viewAll =driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[2]/div[8]/div[2]/span/a"));
		  //WebElement viewAll =driver.findElement(By.xpath("//a[contains(text(),'View All Products')]"));
		  WebElement newArrival =driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[2]/div[7]/div/div[2]/div[1]/a"));
		  jse.executeScript("arguments[0].scrollIntoView(true);", newArrival);
		  wait.until(ExpectedConditions.elementToBeClickable(newArrival));
		  if(newArrival.isDisplayed()&&newArrival.isEnabled()) {
		  newArrival.click();
		  }
		  
	  }
	  
	  By selectdropdown=By.xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div");
	  By price= By.xpath("//a[@class='na'][contains(text(),'Price')]");
	  By priceHighToLow= By.xpath("//a[contains(text(),'Price: High To Low')]");
	  
	  Actions action= new Actions(driver);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(selectdropdown));
  
		  
		  if(driver.findElement(selectdropdown).isDisplayed()&&driver.findElement(selectdropdown).isEnabled()){
			  action.moveToElement(driver.findElement(selectdropdown)).moveToElement(driver.findElement(price)).click().perform();
			  wait.until(ExpectedConditions.visibilityOfElementLocated(priceHighToLow));
			  action.moveToElement(driver.findElement(selectdropdown)).moveToElement(driver.findElement(priceHighToLow)).click().perform();
	  }

	  
	  jse.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id=\"maindiv\"]/div[1]/div/div[1]/div[2]/a")));
	 utils.HelperFunctions.captureScreenShot(driver, filename); 
	  
	  
	  List<WebElement> welist=driver.findElements(By.xpath("//*[@class=\"r1 B14_42\"]/a"));
	  ArrayList<String> arr = new ArrayList<String>();
	  ArrayList<String> arr2 = new ArrayList<String>();
	  System.out.println("First 8 prices of "+data+"  search are-");
	  for(int i=0; i<8; i++) {
		  System.out.println("-->"+welist.get(i).getText());
		  arr.add(welist.get(i).getText());
		  }
	  arr2.addAll(arr);
	  Collections.sort(arr, Collections.reverseOrder());
	  /*
	  Iterator<String> itr = arr.iterator();
	  while(itr.hasNext()) {
		  System.out.println("-itr->"+itr.next());
	  }  
	  */

		if(arr.equals(arr2)) {
			System.out.println("First 8 prices of "+data+" are in decending order");
			System.out.println("------------------------------------");
		}else {
			System.out.println("First 8 prices of "+data+" are not in decending order");
			System.out.println("------------------------------------");
		}
		
}
	  
	    
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
	  ChromeOptions options=new ChromeOptions();
	  options.addArguments("--disable-notifications");
	  options.addArguments("--disable-popup-blocking");
	  driver=new ChromeDriver(options);
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  wait= new WebDriverWait(driver, 30);
	  jse = (JavascriptExecutor) driver;  
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }
  
  @DataProvider
  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
	  String[][] data;
		
	   data = utils.XLDataReaders.getExcelData("src\\test\\resources\\Data\\firstcrydata.xlsx", "data");
       return data;
	  
  }

}
