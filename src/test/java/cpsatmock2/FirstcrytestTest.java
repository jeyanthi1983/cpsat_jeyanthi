package cpsatmock2;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class FirstcrytestTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
	  System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void firstcrytest() {
    driver.get("https://www.firstcry.com/");
    driver.manage().window().setSize(new Dimension(874, 728));
    {
      WebElement element = driver.findElement(By.id("imgb1"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    driver.switchTo().frame(15);
    driver.switchTo().defaultContent();
    driver.findElement(By.id("search_box")).click();
    driver.findElement(By.id("search_box")).click();
    driver.findElement(By.id("search_box")).sendKeys("diaper");
    driver.findElement(By.cssSelector(".search-button")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".sort-select-content"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.linkText("Price"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.linkText("Price")).click();
    driver.findElement(By.linkText("Price: Low To High")).click();
    js.executeScript("window.scrollTo(0,122)");
    js.executeScript("window.scrollTo(0,936)");
    driver.findElement(By.linkText("Price")).click();
    driver.findElement(By.cssSelector(".sort-select-content")).click();
    driver.findElement(By.linkText("Price: High To Low")).click();
    driver.findElement(By.cssSelector(".logo_head")).click();
    driver.findElement(By.id("search_box")).sendKeys("clocks");
    driver.findElement(By.id("search_box")).sendKeys(Keys.ENTER);
    js.executeScript("window.scrollTo(0,207)");
    js.executeScript("window.scrollTo(0,305)");
    driver.findElement(By.linkText("Price")).click();
    driver.findElement(By.linkText("Price: High To Low")).click();
    js.executeScript("window.scrollTo(0,204)");
    driver.findElement(By.cssSelector(".logo_head")).click();
    driver.findElement(By.id("search_box")).sendKeys("shoes");
    driver.findElement(By.id("search_box")).sendKeys(Keys.ENTER);
    {
      WebElement element = driver.findElement(By.cssSelector(".lblock1:nth-child(3) img"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    driver.findElement(By.cssSelector(".lblock1:nth-child(3) li:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".lblock1:nth-child(3) li:nth-child(4)")).click();
    driver.findElement(By.cssSelector(".lblock1:nth-child(3) li:nth-child(4)")).click();
    driver.findElement(By.cssSelector(".lblock1:nth-child(3) img")).click();
    js.executeScript("window.scrollTo(0,1293)");
    driver.findElement(By.cssSelector(".lblock1:nth-child(1) li:nth-child(1) > a")).click();
    driver.findElement(By.linkText("Price")).click();
  }
}
