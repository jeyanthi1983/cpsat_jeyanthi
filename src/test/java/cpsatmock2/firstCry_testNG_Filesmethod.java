package cpsatmock2;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;


import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

import java.io.IOException;

public class firstCry_testNG_Filesmethod {
	WebDriver Driver;
	 @BeforeTest
	  public void beforeTest() {
		 Driver = utils.HelperFunctions.createAppropriateDriver("Chrome", false);
		 Driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  }
	
	@Test(dataProvider = "dp")
  public void f( String s) {
		String SearchWord = s;
		String url = "https://www.firstcry.com/";
		Driver.get(url);
		By searchbox = By.id("search_box");
		By sortSelect = By.xpath("/html/body/div[5]/div[2]/div/div[2]/div[1]/div[2]/div[1]/div");
		By descendingPrice = By.xpath("/html/body/div[5]/div[2]/div/div[2]/div[1]/div[2]/div[1]/ul/li[4]/a");
		
		WebElement search = Driver.findElement(searchbox);
		search.clear();
		search.sendKeys(SearchWord);
		search.sendKeys(Keys.ENTER);
		
		WebDriverWait wait = new WebDriverWait(Driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(sortSelect));
		
		Driver.findElement(sortSelect).click();
		
		WebElement PriceSort = Driver.findElement(descendingPrice);
		Actions actions = new Actions(Driver);
		actions.moveToElement(PriceSort).click().perform();
		
  }

  @DataProvider
  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
	  String[][] data;
	
		   data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\firstcrydata.xlsx", "data");
	
	 // String[][] data = {{"Avengers", "Joss Whedon", "Chris Evans" },set2};
    return data;
  }
  
  @AfterTest
  public void afterTest() {
	 // Driver.quit();
  }

}
