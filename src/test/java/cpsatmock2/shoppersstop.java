package cpsatmock2;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;

public class shoppersstop {
	WebDriver driver = null;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.addPreference("dom.disable_beforeunload", true);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	@Test
	public void test() {
	
		String url = "https://www.shoppersstop.com/";
		 driver.get(url);
		String ExpectedTitle = "Haute Curry Bags | Haute Curry Ladies Footwear | Shoppers Stop | Shoppers Stop";
		String ActTitle ;
		String ExpectText = "Start Something New";
		String ActText;
		String filename = "C:\\Users\\jeyan\\eclipse-workspace\\mavenCPSATonlineJUL2\\src\\test\\resources\\screenshots\\SocialMediaImage1.png";
		
	    By brands = By.xpath("/html/body/main/nav/div[1]/div/ul/li[8]/a");
	    By hautecurry = By.xpath("/html/body/main/section/div/div/div/ul/li[1]/div/div[2]/a/div/img");
	    By text = By.xpath("/html/body/main/footer/div[2]/div/p");
		By facebook = By.xpath("/html/body/main/footer/div[4]/div[1]/div/div/div/div[3]/ul/li[*]/a");
	   
		driver.findElement(brands).click();
	
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		
		WebElement HauteCurry = driver.findElement(hautecurry);
		HauteCurry.click();
		utils.HelperFunctions.captureScreenShot(driver, filename);
		ActTitle = driver.getTitle();
		System.out.println( ActTitle);
		if(ActTitle.contains(ExpectedTitle)) {
			System.out.println("verified Title. " + ActTitle);
		}
		try {
			Assert.assertEquals(ActTitle, ExpectedTitle);
		}catch (Exception e) {
			System.out.println("Can't verify Title. " + e.getMessage());	
			}
		WebElement Text = driver.findElement(text);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Text);
		ActText = Text.getAttribute("innerText");
		System.out.println (ActText);
		
		Boolean isPresent = Text.isDisplayed();
		System.out.println (isPresent);
		if((ActText.contains(ExpectText))) {
			System.out.println ("The Text "+ ActText +" is present");
		}
		
		List<WebElement> FaceBook = driver.findElements(facebook);
		for(WebElement li : FaceBook) {
			String href = li.getAttribute("href");
			System.out.println("The Link to social media is " + href);
		}
		
	}

}
