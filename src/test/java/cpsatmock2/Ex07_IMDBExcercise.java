package cpsatmock2;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Ex07_IMDBExcercise {

	WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
	}

	@After
	public void tearDown() throws Exception {
		
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		String url = "https://www.imdb.com/";
		String MovieName = "Gangs of New York";
		String userReviewTitle = "Gangs of New York (2002) - Gangs of New York (2002) - User Reviews - IMDb";
		
		By searchBox = By.id("suggestion-search-button");
		By byvar = By.id("suggestion-search");
		By FirstLink = By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a");
		By subText = By.xpath("//*[@id=\"title-overview-widget\"]/div[1]/div[2]/div/div[2]/div[2]/div");
		By moviewTime = By.xpath("//*[@id=\"title-overview-widget\"]/div[1]/div[2]/div/div[2]/div[2]/div/time");
		By moviewgenre	= By.xpath("//*[@id=\"title-overview-widget\"]/div[1]/div[2]/div/div[2]/div[2]/div/a[1]");
		By userReview = By.xpath("//*[@id=\"quicklinksMainSection\"]/a[3]");
		
		driver.get(url);
		WebElement elemnt = driver.findElement(byvar);
		elemnt.clear();
		elemnt.sendKeys(MovieName);
		elemnt.sendKeys(Keys.ENTER);
		
			
		WebElement firstlink = driver.findElement(FirstLink);
		firstlink.click();								
		
		
		WebElement SubTextList = driver.findElement(subText);
		WebElement movietime = driver.findElement(moviewTime);
		String Str = SubTextList.getText();
				
		System.out.println(Str);
		if(Str.contains("R"))
			System.out.println(" MPAA rating of the movie is 'R'");
		else
			System.out.println("Movie rating Can't verified ");
		
		
		String time = movietime.getAttribute("innerText");
		//int Time = Integer.parseInt(time);
		
		String time1 = time.substring(0, 1);
		String time2 = time.substring(3,5);
		
		int Time1 = Integer.parseInt(time1) * 60;
		int Time2 = Integer.parseInt(time2);
		int FinalTime = Time1 + Time2 ;
		System.out.println("Time in minutes is " +FinalTime);
		if(FinalTime < 180)
			System.out.println("Verified that the Movie Time is less than 180 min ");
		else
			System.out.println("Movie Time Can't verified ");
		
		WebElement MovieGenre = driver.findElement(moviewgenre);
		String GenreCrime = MovieGenre.getAttribute("innerText");
		System.out.println(" Genre text is "+GenreCrime);
		if(GenreCrime.contains("Crime"))
			System.out.println(" Movie Genre - crime is verified ");
		else
			System.out.println(" Movie Genre can't be verified ");
		
		//*[@class="inline" and contains(text(),'Director')]/following-sibling::*
	}

}
