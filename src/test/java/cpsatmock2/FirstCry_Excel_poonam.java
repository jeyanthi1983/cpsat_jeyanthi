package cpsatmock2;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class FirstCry_Excel_poonam {
	WebDriver driver=null;
	WebDriverWait wait=null;
	
	
  @Test(dataProvider = "dp")
  public void firstCry(String data) {
	  String expURLForShoeSearch="https://www.firstcry.com/baby-kids-footwear";
	  
	  driver.get("https://www.firstcry.com/");
	  driver.findElement(By.xpath("//*[@id=\"search_box\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"search_box\"]")).sendKeys(data, Keys.ENTER);
	  
	  String actualUrl=driver.getCurrentUrl();
	  //System.out.println("Actual Current URL is:   "+actualUrl);
	  
	  if(actualUrl.contains(expURLForShoeSearch)) {
		  
		  JavascriptExecutor jse = (JavascriptExecutor) driver;    
		  WebElement viewAll =driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[2]/div[8]/div[2]/span/a"));
		  jse.executeScript("arguments[0].scrollIntoView(true);", viewAll);
		  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'View All Products')]")));
		  viewAll.click();
	  }
	  
	  By selectdropdown=By.xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div");
	  By price= By.xpath("//a[@class='na'][contains(text(),'Price')]");
	  By priceHighToLow= By.xpath("//a[contains(text(),'Price: High To Low')]");
	  Actions action= new Actions(driver);
	  action.moveToElement(driver.findElement(selectdropdown)).moveToElement(driver.findElement(price)).click().perform();
	  action.moveToElement(driver.findElement(selectdropdown)).moveToElement(driver.findElement(priceHighToLow)).click().perform();
	  
	  /*
	  List<WebElement> welist=driver.findElements(By.xpath("//*[@id=\"maindiv\"]/div[*]/div/div[1]/div[3]/span[1]/a"));
	  for(WebElement we:welist) {
		  System.out.println(we.getText());		  
	  }  */
		  
	  }


  
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
	  ChromeOptions options=new ChromeOptions();
	  options.addArguments("--disable-notifications");
	  options.addArguments("--disable-popup-blocking");
	  driver=new ChromeDriver(options);
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  wait= new WebDriverWait(driver, 30);
	  
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }
  
  @DataProvider
  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
	  String[][] data;
		
	   data = utils.XLDataReaders.getExcelData("src\\test\\resources\\Data\\firstcrydata.xlsx", "data");
       return data;
	  
  }

}
